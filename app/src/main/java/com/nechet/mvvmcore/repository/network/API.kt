package com.nechet.mvvmcore.repository.network

import com.nechet.mvvmcore.repository.response.ArtistsResponse
import com.nechet.mvvmcore.repository.response.TopAlbumsResponse
import com.nechet.mvvmcore.BuildConfig
import com.nechet.mvvmcore.repository.response.AlbumInfoResponse
import retrofit2.http.GET
import retrofit2.http.Query

interface API {
    @GET("?method=artist.search&format=json&api_key=${BuildConfig.API_KEY}")
   suspend fun getArtistsList(
        @Query("artist") artist: String? = null,
        @Query("page") page: Int,
        @Query("limit") limit: Int
    ): ArtistsResponse

    @GET("?method=artist.gettopalbums&format=json&api_key=${BuildConfig.API_KEY}")
    fun getTopArtistAlbums(
        @Query("artist") artist: String? = null,
        @Query("page") page: Int,
        @Query("limit") limit: Int
    ): TopAlbumsResponse

    @GET("?method=album.getinfo&format=json&api_key=${BuildConfig.API_KEY}")
    fun getAlbumInfo(
        @Query("artist") artist: String? = null,
        @Query("album") albumName: String? = null
    ): AlbumInfoResponse
}