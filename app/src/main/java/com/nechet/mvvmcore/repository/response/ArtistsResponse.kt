package com.nechet.mvvmcore.repository.response

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Artist(
    val name: String? = null,
    val listeners: String? = null,
    val url: String? = null,
    val streamable: String? = null,
    @Json(name = "image")
    var images: List<Image>? = null,
)


@JsonClass(generateAdapter = true)
data class Artistmatches(
    @Json(name = "artist")
    val artists: List<Artist>? = null
)

@JsonClass(generateAdapter = true)
data class ArtistsResponse(
    val results: Results? = null
)

@JsonClass(generateAdapter = true)
data class OpensearchQuery(
    @Json(name = "#text")
    val text: String? = null,
    val role: String? = null,
    val searchTerms: String? = null,
    val startPage: String? = null
)

@JsonClass(generateAdapter = true)
data class Results(
    @Json(name = "opensearch:Query")
    val opensearchQuery: OpensearchQuery? = null,
    @Json(name = "opensearch:totalResults")
    val opensearchTotalResults: String? = null,
    @Json(name = "opensearch:startIndex")
    val opensearchStartIndex: String? = null,
    @Json(name = "opensearch:itemsPerPage")
    val opensearchItemsPerPage: String? = null,
    val artistmatches: Artistmatches? = null
)