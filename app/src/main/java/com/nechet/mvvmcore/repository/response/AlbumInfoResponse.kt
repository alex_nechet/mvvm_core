package com.nechet.mvvmcore.repository.response

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass


@JsonClass(generateAdapter = true)
data class AlbumInfoResponse(
    val album: AlbumInfo? = null
)

@JsonClass(generateAdapter = true)
data class AlbumInfo (
    val artist: String? = null,
    val name: String? = null,
    val playcount: Int? = null,
    val url: String? = null,
    val images: List<Image>? = null,
    val tracks: Tracks? = null
)

@JsonClass(generateAdapter = true)
data class Track (
    val name: String? = null,
    val url: String? = null,
    val duration: String? = null,
    val artist: Artist? = null
)

@JsonClass(generateAdapter = true)
data class Tracks (
    @Json(name = "track")
    var trackList: List<Track>? = null
)

