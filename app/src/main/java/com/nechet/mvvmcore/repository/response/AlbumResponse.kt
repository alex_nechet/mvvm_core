package com.nechet.mvvmcore.repository.response

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Album(
    val artist: Artist? = null,
    val name: String? = null,
    val playcount: Int? = null,
    var mbid: String? = null,
    val url: String? = null,
    @Json(name = "image")
    val images: List<Image>? = null,
    val tracks: Tracks? = null
)

@JsonClass(generateAdapter = true)
data class Image(
    @Json(name = "#text")
    val text: String? = null,
    val size: String? = null
)

@JsonClass(generateAdapter = true)
data class TopAlbumsResponse(
    val topalbums: Topalbums? = null
)

@JsonClass(generateAdapter = true)
data class Topalbums(
    @Json(name = "album")
    val albums: List<Album>? = null
)