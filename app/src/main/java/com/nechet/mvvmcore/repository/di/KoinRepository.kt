package com.nechet.mvvmcore.repository.di

import com.nechet.mvvmcore.BuildConfig
import com.nechet.mvvmcore.repository.network.API
import com.squareup.moshi.Moshi
import com.squareup.moshi.adapters.Rfc3339DateJsonAdapter
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import java.util.*

object KoinRepository {
    val repositoryModule = module {
        single {
            val interceptor = HttpLoggingInterceptor().apply {
                level = when {
                    BuildConfig.DEBUG -> HttpLoggingInterceptor.Level.HEADERS
                    else -> HttpLoggingInterceptor.Level.NONE
                }
            }
            OkHttpClient.Builder().addInterceptor(interceptor).build()
        }

        single {
            Moshi.Builder()
                .add(Date::class.java, Rfc3339DateJsonAdapter().nullSafe())
                .build()
        }

        single { MoshiConverterFactory.create(get()) }

        single {
            Retrofit.Builder()
                .baseUrl(BuildConfig.BASE_URL)
                .client(get())
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(get())
                .build()
                .create(API::class.java)
        }
    }
}