package com.nechet.mvvmcore.presentation.di

import com.nechet.mvvmcore.presentation.main.MainViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

object KoinPresentation {

    val viewModelsModule = module {
        viewModel { MainViewModel() }
    }
}