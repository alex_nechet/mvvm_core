package com.nechet.mvvmcore.common.di

import com.nechet.mvvmcore.presentation.di.KoinPresentation
import com.nechet.mvvmcore.repository.di.KoinRepository

object Koin {

    val modules = listOf(
        KoinPresentation.viewModelsModule,
        KoinRepository.repositoryModule
    )
}